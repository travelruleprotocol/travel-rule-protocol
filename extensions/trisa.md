# TRISA Extensions

## Goal

This extension provides a mechanism for VASPs who implement the TRP protocol to communicate with VASPs who implement the [TRISA](https://trisa.io) protocol. The OpenVASP Association and the TRISA Working Group are both open, non-profit working groups interested in facilitating Travel Rule compliance for Virtual Asset Service Providers. In an effort to extend both of our networks, OpenVASP and TRISA collaborated to create a [TRISA/TRP Bridge](https://trisa.dev/openvasp/) that would allow TRISA node implementers to communicate with TRP nodes.

The TRISA extensions allow TRP payloads to contain _sealed_ or _unsealed_ [Secure Envelope](https://trisa.dev/data/envelopes/) payloads.

- **Sealed Payloads**: contain key material that are encrypted with the public key of the recipient
- **Unsealed Payloads**: contain key material that are unencrypted

TRP nodes that implement the TRISA extension may use the service they've already created for TRP to engage the TRISA protocol to:

- Perform TRISA-style non-repudiation
- Securely store counterparty PII during the required compliance storage period
- Automatically erase stored PII by deleting keys rather than data.

## Extension Headers

The extension headers used in the v1 spec would be either:

```
api-extensions: sealed-trisa-envelope,some-other-extension
```

or

```
api-extensions: unsealed-trisa-envelope,some-other-extension
```

Note that the `sealed-trisa-envelope` and `unsealed-trisa-envelope` should be considered mutually exclusive extensions.

## mTLS

TRISA nodes require mTLS, which is part of the [v1 core spec](https://gitlab.com/OpenVASP/travel-rule-protocol/-/blob/master/core/specification.md?ref_type=heads#client-authentication). However, most TRISA nodes will only respect the TRISA Certificate Authority for counterparties since certificates are only issued to VASPs who are verified by a TRISA KYC process that determines if the VASP must exchange PII data to comply with the travel rule.

To sign up for your free TRISA mTLS certificates, please visit the [TRISA Global Directory Service](https://vaspdirectory.dev) to register and begin the verification process.

## Secure Envelopes

Secure Envelopes are used by the TRISA protocol to encrypt and secure PII that is exchanged between counterparties during Travel Rule compliance information transfers. While transport layer security such as TLS encrypts the communication of the payload in-flight, secure envelopes add a secondary layer of protection as well as a mechanism for storing the PII payload at-rest during the required compliance duration. Secure Envelopes also provide non-repudiation using cryptographic HMAC signatures and allow for other cryptographic guarantees.

For more information please see [the TRISA documentation on Secure Envelopes](https://trisa.dev/data/envelopes/).

### Example Payload

The following is an example of a sealed envelope payload:

```json
{
  "id":  "1e7bb3cc-53ff-41b8-870f-4c22110e6f04",
  "payload":  "OrBvMmcdF2opQxsRGYMh6b9z+WpTKjhhRfis8ZjTkSmHnuoRlF3saF3NTAPIrKPoSA4jaAiEcL/cWa165UfHHu08W+iXgb5eB7scNSgqFPDYVlS1fVbTAUNwedgcoZ3EenF5+t1ky0SQ7ejOzp8FK6l49Meh+UfQf62ELFQi5jBMcaYokvmDJDwgOkJ7rVmZ0NA+LSxyZCoMKbQuiOHapYg1yCgAfVfpazO7d5b54BihZrW7PArpDqQeocpcwCWN4RXJYuHkjRwZmUM3Ks9gKP8tbZgf30giCejxGXgKw02sDplC+9kG8Mfe8XhkaART6NqhDZu+l1ajtI+oFuNM3tJ4XZ+MIDcvA2bs4sCu+g8AILVqNzbHPw36BbmytaFENyYILllMTPSZVxw7FxWCUXfxX0sbCGELHCju2O8Kjdam2v+KHnWM1QeUCOQNLJsXsiWgzYXArlHA1fts/axUcbyyXPYNAyEkH+zRsOsoLmkTVgTdCpFO1QIQ45zZrXZlHZ51q9fa7BBOTnDtFQOkJ6Rzqmuv/gHLq5hFwDdy1/gUOQAYQxvk0daNuBN4n+yLmn7d2Z3iQlEmWteGmU1+vJSY0L/xT0HXeLjdAAbVkA2FxqnBt8ScN48wEhulIeZQLuaIC4h/33Pi643Xy06Q1HEY4IVv9BDR/EeyeszDbr+3+5b0S8XCZmgMZyZL2Qkiny1PVYUbZx/LJAXXYTcE+zGiltGVuiN0AueF/8ymBl95Yr61UZg1YjjdjqaI56yM3Aq13HQZtLL5ccFS8mTN5YsomckQJzb5+J8rW/zcWbUsgYSgHJnv/olPvS18Y3+GjYRqQ0r3/7c3e75otf5GbqcCIwuyrVDEMK14RBG7p6vZYcfP5dNTvVOtoUrqhBze/YlZOVfk0nrQHOytluLKnHnYG5c/MTscSWkE2/r/Bi6MGwzHtrMjY2Q96NqN+G7bQyRiPHSj+aCnMuwNw2/+/rhGAQCz9gW6UbGGvH0uMbyqNtx+oTSnf5as7EbkkRK41LDyUG7AErcx9B+z/4wfFi53mqb52W6orrtEiNIAFhvuXK5mS5fBiWUi4nxrUUnNiAUWvu6CvzHVa8UOXOPsrEawoK4PtjuJO75cKNveFTBq6gh2FFyTLryhu7RkfvIwto1ec8PoFl6togcMiJqBwTuzraXAqh6GRQAp5ELJEWskc1U89UR2GCWnJB8nOp0OCw/wjx/UddFxouZ8aCHY8OzJAA9RgL7lDSSf/HplH+RdRUDMGonKl/FBjJggeGmdWaGFlJW83m+wPuCkxekS7avj1V9btrv7Qpj7qY6F3Pv+rqwbvmZ4hlY/6GI0YeT7ViB9GYbhgMRfkX03jnpH9wnsn7LlM+Dr65yB84rmhGm/Sl6mfXjbD/NOtAdldeIScgy+UnaQlKK1nZmscTe/X4XAZaFfBopEn1WGwByF9yoN5cXpbAZdSH66BYQBQObAToGIrdMjnPqMx8Ju7pDx0+AS/Q/4cgBr0bYeqTgPPcTk1Nko9zK0li+w43WFXi4jpORwj0BQD/qTM2cVuyKJBkJDlHi4mtwN8cpR/kdcRuj8HwZVYMaemDU2BhfH3w==",
  "encryption_key":  "pXpr/U87nsKHaUNQASTtN07Et1K+k0yZ/faQLdhmaUUiUuOh2+ikRNu2FFNBvoV3DcgJ+LVaeXjUz53lgdxr4QSzJZ9iIWJfjSsgSbYRJnw5UbdtNMBwL5CAFHb97Zg0Y1YYETEC1PbMWgYFX9nGhZqt4YBvQ2Jh08T+ct7Ui83RB7YBpOMQs3jYMLbuDFblmzBDBNLK6RVQCBYbg0HdGhl6NBqwC+FEGvHpwIta26ggQ1Mf54KnfK+yTeS2Kc0RaxvkOdpUpSfRzFsOyGiCAh+Smu4m7d+TuU10pNA9GR9yotDv2UDdJhyTNuLzYYsQAWENUmOXdeRtuNk7gAdQ3wDajAjhmia9jKjZM3Yja1ToenIsWqCdwTBoa+QxSlOlR9MB2wUx/BfVGT/IIzCSboTdqu5YRSjdNr4jo6o1oAD0v8v73qUERjzRgoWHepqq0H177tkIe+PrewmJTaZP4fQs4sMwBua6Zjlr+T4ka0NZ1sqptRTr6IO+zbuJPGlgrk7B1wMAqHgitVoMZHr4o+yZX2Zaq+npwIrjjSulGBlPyGGN0mzIuoKE3oxhOMTpBtG9ORzAXvYEw3ADkyqkfB32c+c79TzvcRgFfM2F+0Ig3mxeTAyqRv4EaOzrSZjfCsaQiemnOEBzoOM+iE8RYb/CZEC99JxcQ9V+zAWr9VU=",
  "encryption_algorithm":  "AES256-GCM",
  "hmac":  "vzKqwO072JrDvVaoOZZ9WXjA+BM1n607PFGLEdZ0DAg=",
  "hmac_secret":  "nmQD4a2fAuCzMviIEo59D4tsU2aZbSFZtnT9qrlo/XPCE9vi0GYPfcRwgCEeVtMtjjiOaYHmsbSrw7Z8pZ2l5/V2hARCz1IXA1iUYvDPbRTAWrAOeQ6wd/igzimtB7bBnfe7fHV8BjrsgEpAjKkbkX97KPm+O10tDlf6I8fjpBLaGMk5QmLJPOBianuNRgzMVcadMK4pHBit0I01brGYL5nKAVowF5QbBLRbyHLU/QIw0/aFpjn1GX+jeeSRtDXWQPAEHDn9FnC4zrWB8ctzKHRLp/BPDwXrP3TazUOcEe96jWR+7u++nh8U+T8/+SDeNyH2JglS99nXbVZ8KvqkBBt9wlYpK7KeKQgvx6RzMfVl4j/yP0GAf70AslWUfpd4/s1iBMsho62yrbZ3HS1vqUd+qizNEbK1WX6czDfpyia3vR4gg0UWhfWmLcqZIokvu7FwrK+yWlLQDBVoPvNGpIg6JWCCFrQOpnMRtmDYXRIrAXy7z90ZhuS5Ul7PbLAqWE2nKIzOXl8PdE4UoNolkuO4CC0I1fQyMD+m5sJPpDdcmGGKbd67IGgiuoQAoSF96pl8y4JRfKoS9yQQ+XCaSk4fwS4kMsEEmleIQPJBzH4HKaETx/Rq/4Voy0tY/Io6VU6tbrF+kS2E0bP2uKHbHrR3eSZPX7MnNkSIRf2gkGI=",
  "hmac_algorithm":  "HMAC-SHA256",
  "error":  null,
  "timestamp":  "2022-03-29T09:16:29.755212-05:00",
  "sealed":  true,
  "public_key_signature":  "SHA256:QhEspinUU51gK0dQGqLa56BA6xyRy5/7sN5/6GlaLZw"
}
```

| Field                | Type   | Description                                                                                                                                                                        |
|----------------------|--------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| id                   | string | The envelope ID, should match the request identifier in the header. Generally, a UUID4.                                                                                            |
| payload              | base64 | Base64 encoded encrypted payload data. The payload should be the TRP JSON including the asset, amount, and IVMS101. It should be encrypted using the encryption key and algorithm. |
| encryption_key       | base64 | The encryption key used to decrypt the payload. If sealed, this key is encrypted by the recipient's public key.                                                                    |
| encryption_algorithm | string | The name of the encryption algorithm used to encrypt the payload.                                                                                                                  |
| hmac                 | base64 | A digital signature created on the encrypted payload that is used to verify the contents have not been modified and can be used for non-repudiation.                               |
| hmac_secret          | base64 | The hmac secret used to create the digital signature. If sealed, this secret is encrypted by the recipient's public key.                                                           |
| hmac_algorithm       | string | The name of the hmac algorithm and hash/hashing size used to create the hmac.                                                                                                      |
| error                | string | Used to communicate any TRISA-specific errors. See [TRISA error codes](https://trisa.dev/api/errors/) for more information.                                                        |
| timestamp            | string | The RFC3339Nano formatted timestamp of the time the envelope was created.                                                                                                          |
| sealed               | bool   | Indicates if the encryption_key and hmac_secret are encrypted or in the clear.                                                                                                     |
| public_key_signature | string | Used to help the recipient identify the public key that was used to seal the envelope.                                                                                             |

### Sealed vs Unsealed Envelopes

Payloads are encrypted and signed using symmetric encryption algorithms such as [AES256-GCM](https://en.wikipedia.org/wiki/Galois/Counter_Mode) and [HMAC-SHA256](https://en.wikipedia.org/wiki/HMAC). A random encryption key and hmac secret is generated on a per-payload basis for these algorithms and attached to the secure envelope.

An unsealed envelope contains an encryted payload but the encryption key and hmac secret are in the clear; meaning all of the data necessary to decrypt the message is in the clear. This mode of operation may be used when the receipient's public key is not known or not available. Security of the message in transport is guaranteed by mTLS but it is expected that the recipient securely stores the encryption key and hmac secret either in a [secrets vault](https://cheatsheetseries.owasp.org/cheatsheets/Secrets_Management_Cheat_Sheet.html) or by encrypting the key and hmac secret with their public key upon receipt.

A sealed envelope encrypts the encryption key and hmac secret before the envelope is sent using the _recipient's_ public key. This means that a key exchange is required before the sealed envelope can be sent to the recipient.

## Key Exchange

In order to facilitate the exchange of secure envelopes, it is necessary for counterparties to be able to exchange public keys. TRP Nodes should host an endpoint that allows the exchange of public key material.

The key exchange endpoint should allow the transfer of x509 certificates that indicate the public key algorithm that should be used to seal secure envelopes. A simplified JSON structure of an x509 certificate is used to enable the transfer.

### Endpoint

```
GET /key-exchange
```

Optionally:

```
GET /key-exchange?vaspID=[GDS vaspID of requester]
```

### Response

```json
{
  "version": 3,
  "signature": "zbqC9Hb6w+LCbFd5m9omJ/l+4XJLKErCm9SQ4SV8tvrQ+m3MPP3gHiTfR4XymBpMHtWn77K0UQDgW2rJua7FJHlIX/gbQQ940C4TZ+yzPCloGcHNVX8vKfCBPSYcK868uQVf16zTR3i0jnBtvt/wjsaVjLLRyXUQeWul/tq//OOgmbyyo03lt8sgspKdbrQutuhJ2qkxaGGBQiDyG2jmLCR5ng9Q6d700YiuJBfjFLtRJ7aKYDN7wMDLlIfOja+m6en8zcc/rbAkHc06f33x90dFPk/+IDLszv9DAAv2i06oVjDf1iohNpwIg9Q8Lt2Zt+l3yA96oDIoyuZqrSwSHrb3eJYBv5yi3F9LxGuFco3HS+EGYMgfrJIsmA2mU94EUDa7ndoDllftE/3lLGQFnpmqCuwze7zUj78CfN++1o5UAE4GzKtyA35Jhx2whzqrAWMh1oZ/NXuyOOuQfd0R+e10DKq3FHwccITGc7NaTw0vfVeb3LRlqdfX8Njino3xU5hCSYOAEeF6ZCPCp61FdhBkV6Oy8ApAnOz87OnvjK4kkC+uFEA1yo+CLUpJd+8Tv6uN6bFz86gj0HJT0ivhSpZcNKA7qu3vsOl+O1o1M+w2dY/u3Pe9XwWlNW/TDiN951h8Gol8MIALhPVLmC2S7E8oJ3asOIZ2Xr9p8Q0u+SQ=",
  "signature_algorithm": "SHA256-RSA",
  "public_key_algorithm": "RSA",
  "not_before": "2022-05-15T18:34:12Z",
  "not_after": "2022-11-15T19:34:12Z",
  "data": "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA6EqN4O1gXcfu1I4+OYmif9ZU5fE+suWmlSFmXXdOtaedh+nsnMT6WZCCNuy81xPLMoMOljs3tvmeoN817nUvsFEYbLk1goiO9zAVMQFXF8Av6OWKcloEOQ7YXl88MZEaysPIyNOjc/02TgaLoP/oNCbKOIpvx7uP0rWC7PX/3F/Uczx3MuaIBBUZAU8maJCGYw254Hr1f1rxl0b8f1oywcsE6UQhiBS9/ukKbL/NvYLTTL5ZZznm/qdppORyAuXVxXXWLQQH3D/kaHzOBa8vwrsn3+axlpYDnSEkgNI8HT0qHmUaHYsp7om/EoIRJ3JndIuAk7xjAoxs5+40Le5XUZWrEKDhUIRbg3s01+Qds4WCO8dhklCFaXXGnYS+ROscqGmelCfFtjMY37i9cnEGJHlRJkGE+huq8GcWIRcLldExVi38HLfZ7dNmPRvDVnd0bQOuDrf5idKM0FuoxtIG8OscU5V3OFphDtely64I+ENN1Rv6Qq9WQSySGl8FLLDqMcOFqlOr3D/m/coq5OwNUF7/5ETf/jkp2XwGiLnDu2JGuL8Jp4U9LqgS8llFAgOxJk8C7VCR/aBNyylnhji0nolUbasxuLa/b3ugxeHrpCch3spA7MTZU/ENGk7GKr9GQR8veEU5T5WOQCDOyMuYYMw5Nv1yx0zPUDN6vSw9FicCAwEAAQ=="
}
```

| Field                | Type   | Description                                                  |
|----------------------|--------|--------------------------------------------------------------|
| version              | int64  | The x509 certificate version                                 |
| signature            | base64 | Base64 encoded signature of the certificate                  |
| signature_algorithm  | string | The algorithm used to compute the signature of the certs     |
| public_key_algorithm | string | The algorithm that should be used to seal secure envelopes   |
| not_before           | string | RFC 3339 formatted x509 not before timestamp                 |
| not_after            | string | RFC 3339 formatted x509 not after timestamp                  |
| data                 | base64 | Base64 encoded public key serialized to PKIX, ASN.1 DER form |