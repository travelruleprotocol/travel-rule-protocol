# Goal

This extension proposes to use [W3C Verifiable Credentials](https://www.w3.org/TR/vc-data-model/) as a modern mechanism to solve two major issues faced by companies implementing the Travel Rule.

* VASP discovery
* Beneficiary name matching

The flow changes to use a Verifiable Credential issued by the Beneficiary VASP instead of the Beneficiary section of the IVMS101 payload. It also relies on the [DID based VASP identification](./did-based-vasp-identification.md) extension.

The flow looks like this:

1. Beneficiary VASP issues VC containing required Beneficiary information to their customer. This can be shared directly to a customer's crypto wallet or as a URL that can be shared by email, message etc
2. Beneficiary customer shares VC with the Originator
3. Originator shares VC with the originating VASP to initiate transfer
4. Originating VASP has the DID of the Beneficiary VASP as well as the Transfer Notification endpoint
5. Originating VASP calls TRP notification endpoint stored in the DID Document with the modified IVMS101 data
6. Beneficiary VASP do not have to perform beneficiary name matching as they can verify the VC was issued by them

## Extension header name

In line with v1 of the spec the name would be: `w3c-vc-beneficiary`.

```
api-extensions: w3c-vc-beneficiary, some-other-extension
```

## Extension JSON payload

The payload  in `extensions` > `w3c-vc-beneficiary` now _replaces_ the regular `ivms101`>`beneficiary`>`beneficiaryPersons` keys. Instead of the JSON `benificiaryPerson` structure one or more Verifiable Credentials are included instead.

## Beneficiary Verifiable Credential

The Verifiable Credential is encoded as [JSON/JWT format](https://www.w3.org/TR/vc-data-model/#json-web-token).

The following are required attributes of the top level JWT:

* `vc` IVMS101 `beneficiaryPerson` data
* `iss` the [DID of the issuing VASP](./did-based-vasp-identification.md)
* `sub` a DID identifying the beneficiary customer, a customer reference or blockchain address used for beneficiary
* `exp` Expiry date encoded as a unix timestamp
* `iat` Issuance timestamp encoded as a unix timestamp
* `nonce` 

The algorithm used to sign the JWT should support one of the verification keys listed in [`DID based VASP identification`](./did-based-vasp-identification.md)

## Example VC

The following is the `claims` set of a JWT (JSON Web Token) see [RFC7519]:(https://tools.ietf.org/html/rfc7519):

```json
{
  "sub": "did:example:ebfeb1f712ebc6f1c276e12ec21",
  "iss": "did:ion:EiClkZMDxPKqD9c-umQfTkR8vvZ9JPhl_xLDI9Nfk38w5w",
  "iat": 1541493724,
  "exp": 1573029723,
  "nonce": "660!6345FSer",
  "vc": {
    "@context": [
      "https://www.w3.org/2018/credentials/v1",
      "https://intervasp.org/wp-content/uploads/2020/05/IVMS101-interVASP-data-model-standard-issue-1-FINAL.pdf"
    ],
    "type": ["VerifiableCredential", "BeneficiaryName"],
    "credentialSubject": {
      "naturalPerson": {
        "name": {
          "nameIdentifier": [
            {
              "primaryIdentifier": "MachuPichu",
              "secondaryIdentifier": "Freddie",
              "nameIdentifierType": "LEGL"
            }
          ]
        }
      }
    }
  }
}
```

## Example payload

```
POST /assets/BTC/transactions
{
  "IVMS101": {
    "originator": {
      "originatorPersons": [
        {
          "legalPerson": {
            "name": {
              "nameIdentifier": [
                {
                  "primaryIdentifier": "Singapore Airlines",
                  "nameIdentifierType": "LEGL"
                }
              ]
            },
            "geographicAddress": [
              {
                "addressType": "GEOG",
                "streetName": "Potential Street",
                "buildingNumber": "24",
                "buildingName": "Weathering Views",
                "postcode": "91765",
                "townName": "Walnut",
                "countrySubDivision": "California",
                "country": "US"
              }
            ],
            "customerNumber": "1002390"
          }
        }
      ],
      "accountNumber": []
    },
    "beneficiary": {
      "accountNumber": [
        "1BVMFfPXJy2TY1x6wm8gow3N5Amw4Etm5h"
      ]
    }
  },
  "extensions": {
    "w3c-vc-beneficiary": {
      "beneficiaryPersons": [
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJkaWQ6ZXhhbXBsZTplYmZlYjFmNzEyZWJjNmYxYzI3NmUxMmVjMjEiLCJpc3MiOiJkaWQ6aW9uOkVpQ2xrWk1EeFBLcUQ5Yy11bVFmVGtSOHZ2WjlKUGhsX3hMREk5TmZrMzh3NXciLCJpYXQiOjE1NDE0OTM3MjQsImV4cCI6MTU3MzAyOTcyMywibm9uY2UiOiI2NjAhNjM0NUZTZXIiLCJ2YyI6eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvMjAxOC9jcmVkZW50aWFscy92MSIsImh0dHBzOi8vaW50ZXJ2YXNwLm9yZy93cC1jb250ZW50L3VwbG9hZHMvMjAyMC8wNS9JVk1TMTAxLWludGVyVkFTUC1kYXRhLW1vZGVsLXN0YW5kYXJkLWlzc3VlLTEtRklOQUwucGRmIl0sInR5cGUiOlsiVmVyaWZpYWJsZUNyZWRlbnRpYWwiLCJCZW5lZmljaWFyeU5hbWUiXSwiY3JlZGVudGlhbFN1YmplY3QiOnsibmF0dXJhbFBlcnNvbiI6eyJuYW1lIjp7Im5hbWVJZGVudGlmaWVyIjpbeyJwcmltYXJ5SWRlbnRpZmllciI6Ik1hY2h1UGljaHUiLCJzZWNvbmRhcnlJZGVudGlmaWVyIjoiRnJlZGRpZSIsIm5hbWVJZGVudGlmaWVyVHlwZSI6IkxFR0wifV19fX19fQ.PbtTxltLh9GQEmePhdEfkudejetHAzbB4gvCcC8-TiI"
      ]
    }
  }
}
```
