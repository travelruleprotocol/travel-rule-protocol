# DID profile for Travel Rule Protocol

## Goal

VASPs today interact with a wide variety of counterparties. Negotiating individual bilateral agreements between each pair of VASP and exchanging credentials and endpoints out of band is not scalable nor desirable.

The goal of this extension is to use the existing emerging set of standards for [W3C Decentralized Identifiers](https://w3c.github.io/did-core/) or (DIDs) to allow VASPs to identify each other.

These standards together with [W3C Verifiable Credentials](https://www.w3.org/TR/vc-data-model/) are also in parallel being proposed by the EU as forming part of EIDAS 2.0 as well as by [GLEIF](https://gleif.org) the global organization managing the issuance of LEI's.

This extension aims to create a simple set of requirements for using DID's with TRP.

See the following [article](https://notabene.id/post/proposed-solution-to-travel-rule-deadlock) for more about this proposal.

## Purpose

DID's can be used for amongst other:

* Verifying the signature of signed messages (eg. JWT's)
* Being the subject of Verifiable Credentials
* Encrypted messages between multiple identified parties

## Use in the Travel Rule Protocol

Initial use cases that are proposed in separate extensions are:

* Identifiying originating and beneficiary VASPs in a IVMS101 method
* Authenticating an Originating VASP when notifying the Beneficiary VASP about a transfer
* Locate TRP endpoints for Beneficiary VASP (see below)
* [Allowing a beneficiary VASP to issue Verifable Credentials to their customers for solving the beneficiary name matching problem](vc-beneficiary-info.md)

Future uses could be:

* Encrypted payloads containing PII
* Identifying transactions from unhosted wallets

# Brief introduction to DID's

DID's are identifiers that can be resolved to different public keys as well as endpoints for communication. This is an example of a DID:

```
did:ion:EiClkZMDxPKqD9c-umQfTkR8vvZ9JPhl_xLDI9Nfk38w5w
```

The `ion` part indicates the method. There are multiple methods used to resolve a DID document, containing keys and endpoints.

This is an example of such an DID document (see [did-core](https://www.w3.org/TR/did-core/#did-document-properties) for specifics):

```json
{
  "id": "did:ion:EiClkZMDxPKqD9c-umQfTkR8vvZ9JPhl_xLDI9Nfk38w5w",
  "verificationMethod": [
    {
      "id": "key-1",
      "type": "X25519KeyAgreementKey2019",
      "controller": "<did>",
      "publicKeyBase58": "<entry-value-public-key-base58btc-encoded>"
    }
  ],
  "keyAgreement": [
    {
      "id": "#encryption-key",
      "type": "X25519KeyAgreementKey2019",
      "controller": "<did>",
      "publicKeyBase58": "<entry-value-public-key-base58btc-encoded>"
    }
  ],
  "service": [
    {
      "id": "#trp-transfer-notice",
      "type": "trp-transfer-notice-v2",
      "serviceEndpoint": "https://example.vasp/trp/notice"
    },
    {
      "id": "#trp-address-query",
      "type": "trp-address-query-v1",
      "serviceEndpoint": "https://example.vasp/trp/address-query"
    }
  ],
  "authentication": [
    "#key-1"
  ]
}
```

## Proposed default DID methods for VASP Identification

There are many DID methods and it can be confusing. We suggest a subset of well understood methods as being required for TRP. This list can be extended by adding MR. But caution should be taken, that adding additional DID methods add additional implementation problems for VASPs, so they should be added conservatively.

Any DID method can be used for identifying data subjects or specific blockchain accounts if needed.

- [`web`](https://w3c-ccg.github.io/did-method-web/) Simple DID resolver for https domains
- [`key`](https://w3c-ccg.github.io/did-method-key/) Simple DID method suitable in particular for end user identity references
- [`ethr`](https://github.com/decentralized-identity/ethr-did-resolver/blob/master/doc/did-method-spec.md) Ethereum based DID method

VASP's are recommended to use `web` or `ethr` to represent themselves as they are easily updateable by VASP's, and `key` or `ethr` to represent their customers as they can be created at scale, but are not updateable. See specific standards documents for more details.

None of the above methods have any direct cost to use. `ethr` does require an on-chain transaction for key rotation or adding additional fields to the DID document.
 
## Required Service Endpoints

Service endpoints can be used to locate specific URL used for different purposes. To support TRP the following should be required:

* `trp-transfer-notice-v2` - The URL of the TRP [Transfer Notification endpoint](https://gitlab.com/employees-of-notabene/travel-rule-protocol/-/blob/e20929506f9aecede3916a1dcffc6b7231a58908/core/specification.md#transfer-notification) **Required**

## Encryption and Verification Key types

To simplify implementation, we recommend a small solid subset of signing and encryption keys that are commonly used in blockchain and decentralized identifier methods.

Verification Keys:

* `Ed25519VerificationKey2018`
* `EcdsaSecp256k1VerificationKey2019`

This list can be extended by the Travel Rule Protocol group, but should be based on keeping a limited number of options with good cross platform library support.

